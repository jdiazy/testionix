package com.juandiaz.textionix.services;

import android.util.Base64;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.juandiaz.textionix.entities.ItemsPojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class ServicioRut {


    private final String TAG = this.getClass().getName();

    public  void sendRut(RequestQueue queue, String rutIn, final SimpleResponse simpleResponse){
        String rut = "";
        try {
             rut = getEncripter(rutIn);
        }catch(UnsupportedEncodingException ue){
            Log.e(TAG,"ERROR UnsupportedEncodingException en ServicioRut.sendRut " + ue.getCause());
        }catch(BadPaddingException bpe){
            Log.e(TAG,"ERROR BadPaddingException en ServicioRut.sendRut " + bpe.getCause());
        }catch(NoSuchAlgorithmException nse){
            Log.e(TAG,"ERROR NoSuchAlgorithmException en ServicioRut.sendRut " + nse.getCause());
         }catch(IllegalBlockSizeException ibe){
            Log.e(TAG,"ERROR IllegalBlockSizeException en ServicioRut.sendRut " + ibe.getCause());
        }catch(InvalidKeySpecException ikse){
            Log.e(TAG,"ERROR InvalidKeySpecException en ServicioRut.sendRut " + ikse.getCause());
        }catch(InvalidKeyException oke){
            Log.e(TAG,"ERROR InvalidKeyException en ServicioRut.sendRut " + oke.getCause());
        }catch(NoSuchPaddingException ne){
            Log.e(TAG,"ERROR NoSuchPaddingException en ServicioRut.sendRut " + ne.getCause());
        }

        if(rut.equals("")){
            simpleResponse.responseNoOk("");
            return;
        }

         String url ="https://sandbox.ionix.cl/test-tecnico/search?rut="+rut;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG,"respuesta servicio " + response);

                        try {
                            JSONArray items = new JSONObject(response).getJSONObject("result").getJSONArray("items");

                            List<ItemsPojo> itemsLista = new ArrayList<ItemsPojo>();
                            for (int i = 0 ; i< items.length(); i++){
                                JSONObject item = items.getJSONObject(i);
                                JSONObject itemDetails = item.getJSONObject("detail");
                                itemsLista.add(new ItemsPojo(item.getString("name"),
                                        itemDetails.getString("email"),
                                        itemDetails.getString("phone_number")));
                            }
                            simpleResponse.responseOk(itemsLista);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG,"respuesta servicio rut " + error.networkResponse + error.getMessage());

            }
        });
        queue.add(stringRequest);
    }

    private String getEncripter(String plainRut) throws UnsupportedEncodingException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, InvalidKeySpecException {

        DESKeySpec keySpec = new DESKeySpec("ionix123456".getBytes("UTF8"));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey key = keyFactory.generateSecret(keySpec);
        byte[] cleartext = plainRut.getBytes("UTF8");
        Cipher cipher = Cipher.getInstance("DES");
        cipher.init(Cipher.ENCRYPT_MODE, key);

        return  Base64.encodeToString(cipher.doFinal(cleartext), Base64.DEFAULT);
    }

}
