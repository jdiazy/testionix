package com.juandiaz.textionix.services;

import com.juandiaz.textionix.entities.ItemsPojo;

import java.util.List;

public interface SimpleResponse {

    void responseOk(List<ItemsPojo> itemsPojos);
    void responseNoOk(String responseError);

}
