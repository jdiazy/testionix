package com.juandiaz.textionix.entities;

public class ItemsPojo {

    private String nombre;
    private String email;
    private String phone_number;

    public ItemsPojo(String nombre, String email, String phone_number) {
        this.nombre = nombre;
        this.email = email;
        this.phone_number = phone_number;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone_number() {
        return phone_number;
    }
}
