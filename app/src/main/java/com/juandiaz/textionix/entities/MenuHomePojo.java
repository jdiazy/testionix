package com.juandiaz.textionix.entities;

public class MenuHomePojo {

    private String nombre;
    private String nombreIcono;


    public MenuHomePojo(String nombre, String nombreIcono) {
        this.nombre = nombre;
        this.nombreIcono = nombreIcono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreIcono() {
        return nombreIcono;
    }

    public void setNombreIcono(String nombreIcono) {
        this.nombreIcono = nombreIcono;
    }
}
