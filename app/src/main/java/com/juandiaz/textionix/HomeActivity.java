package com.juandiaz.textionix;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.juandiaz.textionix.entities.ItemsPojo;
import com.juandiaz.textionix.entities.MenuHomePojo;

import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getName();
    private HomeViewModel menuHomeViewModel;
    private ConstraintLayout btnMenu1;
     private TextView txtHomeVersionApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        configView();
    }

    private void configView(){
        btnMenu1 = findViewById(R.id.btnMenu1);
        txtHomeVersionApp = findViewById(R.id.txtHomeVersionApp);
        txtHomeVersionApp.setText(String.format("v%s", BuildConfig.VERSION_NAME));
        menuHomeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        final Observer<List<MenuHomePojo>> observer = new Observer<List<MenuHomePojo>>() {
            @Override
            public void onChanged(List<MenuHomePojo> menuHomePojos) {
                if(menuHomePojos != null){
                    Typeface iconFont = FontAwesomeManager.getTypeFaceFontAwesome(getApplicationContext(),FontAwesomeManager.FONTAWESOME);
                    for (MenuHomePojo menu: menuHomePojos){
                        ConstraintLayout boton = null;

                        Log.i(TAG,"MENU " + menu.getNombre());
                        if(menu.getNombre().equals(getString(R.string.homeNombreBoton1))){
                             boton = (ConstraintLayout) findViewById(R.id.btnMenu1);
                        }else if(menu.getNombre().equals(getString(R.string.homeNombreBoton2))){
                            boton = (ConstraintLayout) findViewById(R.id.btnMenu2);
                        }else if(menu.getNombre().equals(getString(R.string.homeNombreBoton3))){
                            boton = (ConstraintLayout) findViewById(R.id.btnMenu3);
                        }else if(menu.getNombre().equals(getString(R.string.homeNombreBoton4))){
                            boton = (ConstraintLayout) findViewById(R.id.btnMenu4);
                        }else if(menu.getNombre().equals(getString(R.string.homeNombreBoton5))){
                            boton = (ConstraintLayout) findViewById(R.id.btnMenu5);
                        }else if(menu.getNombre().equals(getString(R.string.homeNombreBoton6))){
                            boton = (ConstraintLayout) findViewById(R.id.btnMenu6);
                        }
                        TextView icon = (TextView) boton.getChildAt(0);
                        TextView tituloMenu = (TextView) boton.getChildAt(1);

                        icon.setTypeface(iconFont);
                        long valIconLong = Long.parseLong(menu.getNombreIcono(),16);
                        icon.setText((char) valIconLong+"");
                        tituloMenu.setText(menu.getNombre());
                    }

                }
            }
        };

        final Observer<List<ItemsPojo>> observerItems = new Observer<List<ItemsPojo>>() {
            @Override
            public void onChanged(List<ItemsPojo> itemsPojos) {
                if(itemsPojos != null && itemsPojos.size() >= 2){
                    createAlertVistaDatos(itemsPojos.get(1));
                }else{
                    Toast.makeText(getApplicationContext(), getString(R.string.homeErrorServicioRut), Toast.LENGTH_LONG).show();
                }
            }
        };
        menuHomeViewModel.getItems().observe(this,observerItems);
        menuHomeViewModel.getMenu().observe(this,observer);
        menuHomeViewModel.initMenu();

        btnMenu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAlertRut();
            }
        });


    }
    private void createAlertVistaDatos(ItemsPojo item){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.homePreguntaAlert));
        builder.setTitle(getString(R.string.homeTituloAlert));
        final TextView textNombre = new TextView(this);
        textNombre.setInputType(TextView.TEXT_ALIGNMENT_CENTER);
        textNombre.setText(item.getNombre());
        final TextView textMail = new TextView(this);
        textMail.setInputType(TextView.TEXT_ALIGNMENT_CENTER);
        textMail.setText(item.getEmail());
        final LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(textNombre);
        linearLayout.addView(textMail);
        builder.setView(linearLayout);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    private void createAlertRut(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.homePreguntaAlert));
        builder.setTitle(getString(R.string.homeTituloAlert));
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setHint(getString(R.string.homeEjemploRutAlert));
        builder.setView(input);
        builder.setCancelable(false);
        builder.setPositiveButton("Ingresar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!input.getText().toString().trim().isEmpty()){
                    RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                    menuHomeViewModel.initItems(queue,input.getText().toString());
                }

            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
