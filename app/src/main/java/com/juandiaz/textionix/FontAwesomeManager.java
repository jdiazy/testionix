package com.juandiaz.textionix;

import android.content.Context;
import android.graphics.Typeface;

public class FontAwesomeManager {
    public static final String ROOT = "fonts/",FONTAWESOME = ROOT+"font_awesome.ttf";

    public static Typeface getTypeFaceFontAwesome(Context context, String font){
        return Typeface.createFromAsset(context.getAssets(),font);
    }

}
