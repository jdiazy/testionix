package com.juandiaz.textionix;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.android.volley.RequestQueue;
import com.juandiaz.textionix.entities.ItemsPojo;
import com.juandiaz.textionix.entities.MenuHomePojo;
import com.juandiaz.textionix.services.ServicioRut;
import com.juandiaz.textionix.services.SimpleResponse;

import java.util.List;

public class HomeViewModel extends ViewModel {
    private MutableLiveData<List<MenuHomePojo>> menu;

    private MutableLiveData<List<ItemsPojo>> items;

    public HomeViewModel(){
        menu = new MutableLiveData<>();
        items = new MutableLiveData<>();
    }

    public LiveData<List<MenuHomePojo>> getMenu(){
        return menu;
    }

    public void initMenu(){
        List<MenuHomePojo> listaMenu = ObtenerMenuUseCase.obtenerMenu();
        if(listaMenu!=null){
            menu.setValue(listaMenu);
        }
    }

    public LiveData<List<ItemsPojo>> getItems(){
        return items;
    }

    public void initItems(RequestQueue queue, String rutIn){
        SimpleResponse simpleResponse = new SimpleResponse() {
            @Override
            public void responseOk(List<ItemsPojo> itemsPojos) {
                items.setValue(itemsPojos);
            }

            @Override
            public void responseNoOk(String responseError) {
                items.setValue(null);
             }
        };
        ServicioRut servicioRut = new ServicioRut();
        servicioRut.sendRut(queue,rutIn,simpleResponse);
    }




}
