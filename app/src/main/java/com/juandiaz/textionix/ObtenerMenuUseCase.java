package com.juandiaz.textionix;

import android.util.Log;

import com.juandiaz.textionix.entities.MenuHomePojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ObtenerMenuUseCase {

    private static final boolean serviceOk = false;
    private static final String respuestaTest ="[{\n" +
            "\"nombre\":\"Pago Fácil\",\n" +
            "\"nombreIcono\":\"f155\"\n" +
            "},{\n" +
            "\"nombre\":\"Estaciones de Servicios\",\n" +
            "\"nombreIcono\":\"f52f\"\n" +
            "},{\n" +
            "\"nombre\":\"Mi Ruta\",\n" +
            "\"nombreIcono\":\"f5a0\"\n" +
            "},{\n" +
            "\"nombre\":\"Mis Beneficios\",\n" +
            "\"nombreIcono\":\"f005\"\n" +
            "},{\n" +
            "\"nombre\":\"Mensajes\",\n" +
            "\"nombreIcono\":\"f0e0\"\n" +
            "},{\n" +
            "\"nombre\":\"Configuración\",\n" +
            "\"nombreIcono\":\"f013\"\n" +
            "}\n" +
            "]";

    public static List<MenuHomePojo> obtenerMenu(){
    if(serviceOk){
        //menu desde el servicio
    }else{
        ArrayList<MenuHomePojo> menu = new ArrayList<MenuHomePojo>();
        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(respuestaTest);

            for (int i=0;i < jsonArray.length();i++){
                JSONObject json = jsonArray.getJSONObject(i);
                menu.add(new MenuHomePojo(json.getString("nombre"),json.getString("nombreIcono")));
            }
            return menu;

        }catch (JSONException e){
            Log.e("ObtenerMenuUseCase","Error en JSON menu. "+e.getMessage());
        }
    }
    return null;
    }

}
